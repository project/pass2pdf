Q: What is pass2pdf?
A: I decided to write pass2pdf based on an idea I had from freelancer.com.
It allows user to register with their own password, then writes the password
to a PDF file and e-mails it to the user.
---

Q: How do I install it?
A: You know the drill.
---

Q: How do I notify you of any bugs, feature requests etc.?
A: Please use the issue queue.
